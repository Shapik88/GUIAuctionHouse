package service;

import entity.BidsEntity;
import repository.BidsRepository;

import java.util.List;

/**
 * Created by Евгений on 08.09.2017.
 */
public class BidsService {
    private final BidsRepository bidsRepository = new BidsRepository();

    public void add(int idProductId, int idBuyerId, int idSellertId, int firstBet, int secondBet) {
        bidsRepository.saveBids(
                BidsEntity.builder()
                        .idProductId(idProductId)
                        .idBuyerId(idBuyerId)
                        .idSellertId(idSellertId)
                        .firstBet(firstBet)
                        .secondBet(secondBet)
                        .build()
        );
    }

    public List<BidsEntity> getBids() {
        return bidsRepository.getBids();
    }


}


