package service;

import entity.ProductEntity;

import repository.ProductRepository;

import java.util.List;

/**
 * Created by Евгений on 07.09.2017.
 */
public class ProductService {
    private final ProductRepository productRepository = new ProductRepository();

    public void addProduct(String productName, int sellerID) {
        productRepository.saveProduct(
                ProductEntity.builder()
                        .productName(productName)
                        .sellerID(sellerID).build()
        );
    }

    public List<ProductEntity> getProducts() {
        return productRepository.getProducts();
    }


}
