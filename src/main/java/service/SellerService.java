package service;

/**
 * Created by Евгений on 07.09.2017.
 */

import entity.SellerEntity;

import repository.SellerRepository;

import java.util.List;

public class SellerService {

    private final SellerRepository sellerRepository = new SellerRepository();

    public void add(String sellerFirstName, String sellerLastName, String sellerNameProduct, String sellerLicenseNumber) {
        sellerRepository.save(
                SellerEntity.builder()
                        .sellerFirstName(sellerFirstName)
                        .sellerLastName(sellerLastName)
                        .sellerNameProduct(sellerNameProduct)
                        .sellerLicenseNumber(sellerLicenseNumber)
                        .build()
        );
    }

    public List<SellerEntity> getSeller() {
        return sellerRepository.getSeller();
    }


}
