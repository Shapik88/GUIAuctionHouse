package service;

import entity.BuyerEntity;

import repository.BuyerRepository;


import java.util.List;

/**
 * Created by Евгений on 08.09.2017.
 */
public class BuyerService {

    private final BuyerRepository buyerRepository = new BuyerRepository();

    public void addBuyer(String buyerName, String buyerLicenseNumber) {
        buyerRepository.saveBuyer(
                BuyerEntity.builder()
                        .buyerName(buyerName)
                        .buyerLicenseNumber(buyerLicenseNumber)
                        .build()
        );
    }

    public List<BuyerEntity> getBuyer() {
        return buyerRepository.getBuyer();
    }


}

