package controller;

/**
 * Created by Евгений on 07.09.2017.
 */

import entity.ProductEntity;
import entity.SellerEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import service.ProductService;
import service.SellerService;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsController {

    private final ProductService productService = new ProductService();

    @FXML
    private TextField productNameField;

    @FXML
    private TextField sellerIDField;


    @FXML
    private TableView<ProductEntity> productTable;

    @FXML
    private TableColumn<ProductEntity, Integer> productIdColumn;

    @FXML
    private TableColumn<ProductEntity, String> productNameColumn;

    @FXML
    private TableColumn<ProductEntity, Integer> sellerIDColumn;


    @FXML
    private void addProduct(ActionEvent event) {
        String productNameFieldText = productNameField.getText();
        Integer sellerIDFieldText = Integer.parseInt(sellerIDField.getText());
        productService.addProduct(productNameFieldText, sellerIDFieldText);
        refreshProductTable();
    }


    public void initialize() {
        productIdColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, Integer>("Product Id"));

        productNameColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, String>("Product Name"));

        sellerIDColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, Integer>("seller ID"));

        refreshProductTable();

    }


    private void refreshProductTable() {
        List<ProductEntity> products = productService.getProducts();
        productTable.setItems(FXCollections.observableArrayList(products));
    }


}

