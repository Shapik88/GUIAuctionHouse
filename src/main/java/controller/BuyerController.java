package controller;

import entity.BuyerEntity;
import entity.ProductEntity;
import entity.SellerEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import service.BuyerService;

import java.util.List;

/**
 * Created by Евгений on 07.09.2017.
 */
public class BuyerController {
    private final BuyerService buyerService = new BuyerService();

    @FXML
    private TextField buyerNameField;

    @FXML
    private TextField buyerLicenseNumber;


    @FXML
    private TableView<BuyerEntity> buyerTable;

    @FXML
    private TableColumn<BuyerEntity, Integer> buyerIdColumn;

    @FXML
    private TableColumn<BuyerEntity, String> buyerNameColumn;

    @FXML
    private TableColumn<BuyerEntity, String> buyerLicenseNumberColumn;


    @FXML
    public void addBuyer(ActionEvent actionEvent) {
        String buyerNameFieldTextField = buyerNameField.getText();
        String buyerLicenseNumberdTextField = buyerLicenseNumber.getText();
        buyerService.addBuyer(buyerNameFieldTextField, buyerLicenseNumberdTextField);
        refreshBuyerTable();
    }


    public void initialize() {

        buyerIdColumn.setCellValueFactory(
                new PropertyValueFactory<BuyerEntity, Integer>("Buyer Id"));
        buyerNameColumn.setCellValueFactory(
                new PropertyValueFactory<BuyerEntity, String>("Name"));
        buyerLicenseNumberColumn.setCellValueFactory(
                new PropertyValueFactory<BuyerEntity, String>("License Number"));
        refreshBuyerTable();

    }

    private void refreshBuyerTable() {
        List<BuyerEntity> buyerEntityList = buyerService.getBuyer();
        buyerTable.setItems(FXCollections.observableArrayList(buyerEntityList));

    }

}

