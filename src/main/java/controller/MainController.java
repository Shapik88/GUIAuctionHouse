package controller;

/**
 * Created by Евгений on 07.09.2017.
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

import java.io.IOException;


public class MainController {

    @FXML
    Pane content;

    @FXML
    private void clickProduct(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("products.fxml"));
        content.getChildren().setAll(root);

    }

    @FXML
    private void clickSellerMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sellers.fxml"));
        content.getChildren().setAll(root);

    }

    @FXML
    private void clickBuyerMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("buyers.fxml"));
        content.getChildren().setAll(root);

    }

    @FXML
    private void clickBidsMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("bids.fxml"));
        content.getChildren().setAll(root);

    }

}