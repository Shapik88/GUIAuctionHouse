package controller;


import entity.BidsEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import service.BidsService;

import java.util.List;

public class BidsController {


    private final BidsService bidsService = new BidsService();

    @FXML
    private TextField idProductIdTextField;

    @FXML
    private TextField idBuyerIdTextField;
    @FXML
    private TextField idSellertIdTextField;

    @FXML
    private TextField firstBetTextField;
    @FXML
    private TextField secondBetTextField;


    @FXML
    private TableView<BidsEntity> bidsTable;

    @FXML
    private TableColumn<BidsEntity, Integer> idBidsColumn;

    @FXML
    private TableColumn<BidsEntity, Integer> idProductIdColumn;

    @FXML
    private TableColumn<BidsEntity, Integer> idBuyerIdColumn;
    @FXML
    private TableColumn<BidsEntity, Integer> idSellertIdColumn;
    @FXML
    private TableColumn<BidsEntity, Integer> firstBetColumn;
    @FXML
    private TableColumn<BidsEntity, Integer> secondBetColumn;


    @FXML
    private void addBids(ActionEvent event) {
        Integer idProductIdTF = Integer.parseInt(idProductIdTextField.getText());
        Integer idBuyerIdTF = Integer.parseInt(idBuyerIdTextField.getText());
        Integer idSellertIdTF = Integer.parseInt(idSellertIdTextField.getText());
        Integer firstBetTF = Integer.parseInt(firstBetTextField.getText());
        Integer secondBetTF = Integer.parseInt(secondBetTextField.getText());
        bidsService.add(idProductIdTF, idBuyerIdTF, idSellertIdTF, firstBetTF, secondBetTF);
        refreshBidsTable();
    }


    public void initialize() {
        idBidsColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("Bids Id"));

        idProductIdColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("Id Product Id"));

        idBuyerIdColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("Id Buyer Id"));
        idSellertIdColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("Id Sellert Id"));
        firstBetColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("First Be"));
        secondBetColumn.setCellValueFactory(
                new PropertyValueFactory<BidsEntity, Integer>("Second Bet"));

        refreshBidsTable();

    }


    private void refreshBidsTable() {
        List<BidsEntity> bidsEntityList = bidsService.getBids();
        bidsTable.setItems(FXCollections.observableArrayList(bidsEntityList));
    }


}

