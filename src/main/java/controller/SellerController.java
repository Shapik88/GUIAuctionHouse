package controller;

/**
 * Created by Евгений on 07.09.2017.
 */

import entity.ProductEntity;
import entity.SellerEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import service.SellerService;

import java.util.List;

public class SellerController {

    private final SellerService sellerService = new SellerService();

    @FXML
    private TextField sellerFirstnameFiled;

    @FXML
    private TextField sellerLastNameField;
    @FXML
    private TextField sellerNameProductFiekd;
    @FXML
    private TextField sellerLicenseNumber;

    @FXML
    private TableView<SellerEntity> sellerTable;

    @FXML
    private TableColumn<SellerEntity, Integer> sellerIdColumn;

    @FXML
    private TableColumn<SellerEntity, String> sellerFirstNameColumn;

    @FXML
    private TableColumn<SellerEntity, String> sellerNameProductColumn;
    @FXML
    private TableColumn<SellerEntity, String> sellerLicenseNumberColumn;

    @FXML
    private void addSeller(ActionEvent event) {
        String sellerFirstnameFiledText = sellerFirstnameFiled.getText();
        String sellerLastNameFieldText = sellerLastNameField.getText();
        String sellerNameProductFiekdText = sellerNameProductFiekd.getText();
        String sellerLicenseNumberText = sellerLicenseNumber.getText();
        sellerService.add(sellerFirstnameFiledText, sellerLastNameFieldText, sellerNameProductFiekdText, sellerLicenseNumberText);
        refreshSellerTable();
    }

    public void initialize() {

        sellerIdColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, Integer>("Id"));

        sellerFirstNameColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("First Name"));

        sellerNameProductColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("Last name"));
        sellerLicenseNumberColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("Product"));
        sellerLicenseNumberColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("License number"));

        refreshSellerTable();

    }

    private void refreshSellerTable() {
        List<SellerEntity> sellers = sellerService.getSeller();
        sellerTable.setItems(FXCollections.observableArrayList(sellers));
    }

}
