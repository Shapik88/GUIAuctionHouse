package entity;

/**
 * Created by Евгений on 07.09.2017.
 */

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SellerEntity {

    private int idSeller;
    private String sellerFirstName;
    private String sellerLastName;
    private String sellerNameProduct;
    private String sellerLicenseNumber;

}
