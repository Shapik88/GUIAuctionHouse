package entity;

/**
 * Created by Евгений on 07.09.2017.
 */

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductEntity {

    private int idProduct;
    private String productName;
    private int sellerID;

}
