package entity;

/**
 * Created by Евгений on 07.09.2017.
 */

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class BuyerEntity {

    private int idBuyer;
    private String buyerName;
    private String buyerLicenseNumber;
    
}
