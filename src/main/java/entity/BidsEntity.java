package entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Created by Евгений on 07.09.2017.
 */
@Builder
@Getter
public class BidsEntity {
    
    private int idBids;
    private int idProductId;
    private int idBuyerId;
    private int idSellertId;
    private int firstBet;
    private int secondBet;
}
