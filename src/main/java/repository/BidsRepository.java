package repository;

import com.google.common.collect.Lists;
import entity.BidsEntity;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * Created by Евгений on 08.09.2017.
 */
@Slf4j
public class BidsRepository extends Repository {

    public void saveBids(BidsEntity bidsEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO bids (product_id, buyer_id, seller_id, first_bet, second_bet) VALUES (?,?,?,?,?)")) {

            preparedStatement.setInt(1, bidsEntity.getIdProductId());
            preparedStatement.setInt(2, bidsEntity.getIdBuyerId());
            preparedStatement.setInt(3, bidsEntity.getIdSellertId());
            preparedStatement.setInt(4, bidsEntity.getFirstBet());
            preparedStatement.setInt(5, bidsEntity.getSecondBet());
            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save seller error!", e);
        }
    }

    public List<BidsEntity> getBids() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT b.id, b.product_id, b.buyer_id, b.seller_id, b.first_bet, b.second_bet FROM bids b")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<BidsEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<BidsEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(BidsEntity.builder()
                    .idBids(resultSet.getInt("Bids Id"))
                    .idProductId(resultSet.getInt("Id Product Id"))
                    .idBuyerId(resultSet.getInt("Id Buyer Id"))
                    .idSellertId(resultSet.getInt("Id Sellert Id"))
                    .firstBet(resultSet.getInt("First Bet"))
                    .secondBet(resultSet.getInt("Second Bet"))
                    .build());
        }
        return result;
    }

}

