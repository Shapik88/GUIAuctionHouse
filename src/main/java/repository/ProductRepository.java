package repository;

/**
 * Created by Евгений on 07.09.2017.
 */

import com.google.common.collect.Lists;
import entity.ProductEntity;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ProductRepository extends Repository {

    public void saveProduct(ProductEntity productEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO product (product_name, seller_id) VALUES (?,?)")) {

            preparedStatement.setString(1, productEntity.getProductName());
            preparedStatement.setInt(2, productEntity.getSellerID());

            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save product error!", e);
        }
    }

    public List<ProductEntity> getProducts() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT p.id, p.product_name, p.seller_id FROM product p")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<ProductEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<ProductEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(ProductEntity.builder()
                    .idProduct(resultSet.getInt("Product Id"))
                    .productName(resultSet.getString("Product Name"))
                    .sellerID(resultSet.getInt("seller ID"))
                    .build());
        }
        return result;
    }

}
