package repository;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import entity.BuyerEntity;


/**
 * Created by Евгений on 08.09.2017.
 */
@Slf4j
public class BuyerRepository extends Repository {
    public void saveBuyer(BuyerEntity buyerEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO buyer (buyer_name, buyer_license_number) VALUES (?,?)")) {

            preparedStatement.setString(1, buyerEntity.getBuyerName());
            preparedStatement.setString(2, buyerEntity.getBuyerLicenseNumber());
            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save seller error!", e);
        }
    }

    public List<BuyerEntity> getBuyer() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT b.id, b.buyer_name, s.buyer_license_number FROM buyer b")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<BuyerEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<BuyerEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(BuyerEntity.builder()
                    .idBuyer(resultSet.getInt("Buyer Id"))
                    .buyerName(resultSet.getString("Name"))
                    .buyerLicenseNumber(resultSet.getString("License Number"))
                    .build());
        }
        return result;
    }

}


