package repository;

/**
 * Created by Евгений on 07.09.2017.
 */

import com.google.common.collect.Lists;
import entity.ProductEntity;
import entity.SellerEntity;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Slf4j
public class SellerRepository extends Repository {

    public void save(SellerEntity sellerEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO seller (seller_first_name, seller_last_name, seller_name_product, seller_license_number) VALUES (?,?,?,?)")) {

            preparedStatement.setString(1, sellerEntity.getSellerFirstName());
            preparedStatement.setString(2, sellerEntity.getSellerLastName());
            preparedStatement.setString(3, sellerEntity.getSellerNameProduct());
            preparedStatement.setString(4, sellerEntity.getSellerLicenseNumber());
            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save seller error!", e);
        }
    }

    public List<SellerEntity> getSeller() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT s.id, s.seller_first_name, s.seller_last_name, s.seller_name_product, s.seller_license_number FROM seller s")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<SellerEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<SellerEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(SellerEntity.builder()
                    .idSeller(resultSet.getInt("id"))
                    .sellerFirstName(resultSet.getString("First Name"))
                    .sellerLastName(resultSet.getString("Last name"))
                    .sellerNameProduct(resultSet.getString("Product"))
                    .sellerLicenseNumber(resultSet.getString("License number"))
                    .build());
        }
        return result;
    }

}

